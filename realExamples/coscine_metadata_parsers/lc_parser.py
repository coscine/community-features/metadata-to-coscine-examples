from datetime import datetime
import xml.etree.ElementTree as ET

from ..iparser import IParser


class LCParser(IParser):
    def parse_file(self, file_path: str) -> dict:
        tree = ET.parse(file_path)
        root = tree.getroot()
        for element in root.iter():
            if element.attrib:
                for key, value in element.attrib.items():
                    if key == "Name" and value == "HyStar_LC_Method":
                        lc_method_value = element.attrib.get("Value", "")
                        self.parsed_metadata["LC_Method"] = lc_method_value
                    if key == "Name" and value == "HyStar_MS_Method":
                        ms_method_value = element.attrib.get("Value", "")
                        self.parsed_metadata["MS_Method"] = ms_method_value

                    if key in [
                        "CreationDate",
                        "CreationTime",
                        "Creator",
                        "CreationComputer",
                        "UpdateDate",
                        "UpdateTime",
                        "UpdateUser",
                        "UpdateComputer",
                        "Author",
                        "Company",
                        "Laboratory",
                        "Phone",
                        "Fax",
                        "EMail",
                        "Description",
                        "BatchID",
                        "Line",
                        "CheckToRun",
                        "Status",
                        "Amount",
                        "Dilution",
                        "Weight",
                        "InternalStandart",
                        "PreRunTime",
                        "DataPath",
                        "SampleType",
                        "ReferenceSampleNum",
                        "NumOnGel",
                        "NumberInSequence",
                        "CalibAction",
                        "CalibLevel",
                        "AquisitionID",
                        "SaltConc",
                        "LastLineInGroup",
                        "InstrumentalCalibration",
                        "RunDAScript",
                        "MBToolsStateDoIt",
                        "MBToolsStateRef",
                        "MBToolsStateDone",
                        "MBToolsLineID",
                        "MBToolsRefLineID",
                        "MBToolsRefInjectionNo",
                        "MBToolsPredicted",
                        "MBToolsIonization",
                        "MBToolsRefLineID2",
                        "MBToolsDiffNo",
                        "MBToolsDiffPath",
                        "StationName",
                        "Version",
                        "Submitter_Language",
                        "Submitter_Login",
                        "Submitter_Name",
                        "Submitter_Supervisor",
                        "SuperMethod",
                        "AutosamplerMethod",
                        "DataPath",
                        "AutosamplerMethod",
                    ]:
                        continue

                    key_mapping = {
                        "NumOfSamples": "Number Of Samples",
                        "SampleTablePath": "Sample Table Path",
                        "FileName": "File Name",
                        "SampleID": "Sample ID",
                        "analysisID": "Analysis ID",
                        "CreationDateTime": "Creation Date Time",
                        "ResultDatafile": "Result Datafile",
                    }

                    if key in key_mapping:
                        key = key_mapping[key]

                    if key == "Injections":
                        value = float(value)

                    self.parsed_metadata[key] = value if len(str(value)) > 0 else None
        self.parsed_metadata = self.parse_dates(self.parsed_metadata)
        return self.parsed_metadata

    def parse_dates(self, parsed_metadata: dict) -> dict:
        date_format = "%Y-%m-%dT%H:%M:%S%z"
        for key, value in parsed_metadata.items():
            if key in ["Creation Date Time"]:
                parsed_metadata[key] = datetime.strptime(value, date_format)
        return parsed_metadata
