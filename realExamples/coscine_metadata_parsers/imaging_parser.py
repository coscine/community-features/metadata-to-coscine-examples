from datetime import datetime

from ..iparser import IParser


class ImagingParser(IParser):
    def parse_file(self, file_path: str) -> dict:
        """
        Parse the file at the given path and return a dictionary of metadata that can be inserted
        into a Coscine MetadataForm

        Args:
            file_path (str): The path to the file to parse

        Returns:
            dict: A dictionary of metadata that can be inserted into a Coscine MetadataForm

        Raises:
            FileNotFoundError: If the file at the given path does not exist
        """
        with open(file_path, "r") as file:
            for line in file:
                line = line.strip()
                if line:
                    key_value = line.split(": ", 1)
                    if len(key_value) == 2:
                        key, value = key_value

                        # this key is not used in the application profile
                        if key in [
                            "Laser Power",
                            "FlexImaging Info File",
                            "Instrument Serial Number:",
                            "Spectrum Size",
                            "Detector Gain",
                        ]:
                            continue

                        # this key spelled differently in the application profile
                        if key == "DataPoints":
                            key = "Data Points"

                        if key == "Datfile Size":
                            key = "Datafile Size"

                        if key == "Acquisition Mode":
                            value = value.capitalize()

                        if key in [
                            "Number of Spots",
                            "Number of Shots",
                            "Sample Rate",
                            "Data Points",
                        ]:
                            value = float(value)

                        self.parsed_metadata[key] = value
                    else:
                        # Handle lines without the expected key-value format
                        # For example, you can choose to skip or process them differently
                        print(f"Ignoring line: {line}")
        self.parsed_metadata = self.parse_dates(self.parsed_metadata)
        return self.parsed_metadata

    def parse_dates(self, parsed_metadata: dict) -> dict:
        date_format = "%a, %d.%m.%Y %H:%M:%S"
        for key, value in parsed_metadata.items():
            if key in ["Start Time", "End Time"]:
                parsed_metadata[key] = datetime.strptime(value, date_format)
        return parsed_metadata
