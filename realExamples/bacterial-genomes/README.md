# Uploading bacterial genomes files and their metadata to Coscine

In this use-case, the research data are genomes of bacterial isolates that are essentially text files distributed with the [FASTA format](https://en.wikipedia.org/wiki/FASTA_format). Here are the following steps we took to transfer data and metadata to Coscine:

1. Describe research data with Application Profile
2. Organize data structure on Coscine
3. Upload data and metadata


## Describe research data with Application Profile

Starting from a selection of metadata, namely the [MIxS](https://genomicsstandardsconsortium.github.io/mixs/MIGSBacteria/) for "Minimal Information about any (x) sequences", we contributed additional metadata with the help of a metadata working group within the German NFDI National Research Data Infrastructure: [NFDI4Microbiota](https://nfdi4microbiota.de).

Using this list, we were able to create a new Application Profile, namely ["fasta"](https://purl.org/coscine/ap/fasta). More details on the Application profiles and how to create them can be found in the [Coscine documentation](https://docs.coscine.de/en/rdm/metadata/about/).

## Organize data structure on Coscine

The bacterial genomes are processed files that are generated within the course of a scientific project. Therefore, the overarching organisational unit in Coscine would be the project (more info in the [Coscine documentation](https://docs.coscine.de/en/projects/create/)).

Initially, we wanted to have one Resource per bacterial isolate genomes, but the minimal size of a Resource on Coscine is 1 GB and the sizes of bacterial genomes files range from 1.5 Mb to 8 Mb, so this strategy would have been a waste of resources (no pun intented).

Therefore, all the genomes files of a project would be deposited in a single Resource that is described using the fasta Application Profile. We settle for the RDS-S3 type of Resource as we wanted a programmatic access for deposition and retrieval. You can learn more about the other types of Resources in the [Coscine documentation](https://docs.coscine.de/en/rdm/resources/about/).


## Upload data and metadata

As we needed a programmatic access to our Resource, we set out to use the [Coscine Python SDK](https://git.rwth-aachen.de/coscine/community-features/coscine-python-sdk) (for Software Development Kit). The steps described below owe a lot to its [documentation](https://coscine.pages.rwth-aachen.de/community-features/coscine-python-sdk/coscine.html).

### Set-up the environment

We work usually within conda/mamba environments, therefore the safest and most reliable way to install a Python package that is on Python Package Index (PyPi), such as Coscine Python SDK, but *within* a conda environment is the following:

```bash
# Based on https://stackoverflow.com/a/56889729
# Install the conda environment with conda/mamba
conda create -n coscine -c conda-forge boto3 pip # "python>3.7" if needed
# Enter the environment
conda activate coscine
# Install the package with the pip module of the conda environment
python -m pip install coscine
# Versions can be specified as well
python -m pip install "coscine>=0.8.2"
```

The associated script [`genomes-coscine-data-upload.py`](genomes-coscine-data-upload.py) details all the steps taken to upload the data. But a couple them will be highlighted below.

### Create the Resource

Once authentified using a Coscine Token, we could create the Resource by providing the necessary metadata for Resource creation using a Python Dictionary. 

```python
resource_metadata = {
    "Resource Name": "genomes",
    "Display Name": "genomes",
    "Resource Type": "rdss3rwth",
    "Resource Size": 1,
    "Resource Description": "The genomes of the bacterial collection in gzipped FASTA format.",
    "Discipline": "Microbiology, Virology and Immunology 204",
    "Metadata Visibility": "Public",
    "Application Profile": "fasta",
}
```

### Assess the requirements for metadata formatting

We created the Resource and the associated metadata form that is displayed below

```python
# The Resource being already created it is just loaded
genomes = prj.resource("genomes")
# Fetch the Application profile and metadata form
new_fasta = genomes.metadata_form()
# 
# +---+------------------------------------------------+-------+
# | C | Property                                       | Value |
# +---+------------------------------------------------+-------+
# |   | FASTQ accession*                               |       |
# |   | Marker for taxonomic identification*           |       |
# | X | Assembly Quality*                              |       |
# |   | Assembly Software*                             |       |
# |   | Coverage*                                      |       |
# |   | Number of Contigs*                             |       |
# |   | N50                                            |       |
# |   | Large ribosomal Sub-Unit (e.g. 23S) recovered* |       |
# |   | LSU Recover Software*                          |       |
# |   | Small ribosomal Sub-Unit (e.g. 16S) recovered* |       |
# |   | SSU Recover Software*                          |       |
# |   | Number of tRNAs*                               |       |
# |   | tRNAs Extraction Software*                     |       |
# |   | Completeness*                                  |       |
# |   | Completeness Software*                         |       |
# |   | Contamination*                                 |       |
# +---+------------------------------------------------+-------+
```


The field `Assembly Quality` is somewhat controlled but not with a dedicated vocabulary that mapped to PIDs.

```python
new_fasta.has_vocabulary("Assembly Quality")  # False
new_fasta.has_selection("Assembly Quality")  # True
```

This metadata field accepts "only" a selection of entries that can be listed below:

```python
form.selection("Assembly Quality")
# ['Finished genome', 'High-quality draft genome', 'Medium-quality draft genome', 'Low-quality draft genome', 'Genome fragment(s)']
```
Therefore, all values of this field would need to comply to elements of this list, and all others fields marked with an asterisk are mandatory.

**Note**: To correctly assigned the metadata values and solve other problems, we wrote a couple of helpers functions in Python, for instance, one to evaluate the assembly quality or find the correct folder in which the data was stored.

### Fetch metadata to fill the metadata form at scale

The workflow *creating* the genomes files results in the creation of a rich-metadata table (see an example in the [workflow repository](https://git.rwth-aachen.de/clavellab/genome-assembly#output-table-metadata-details)). The metadata table is then read and formatted by a custom helper function (`get_metadata_from_df`) to easily fill-out the metadata form of new research data in the Resource.

This scalable approach allowed us to upload 230 genomes files and their metadata on Coscine in a streamlined manner.

```python
# Upload strategy to Coscine
# from the dataframe of genomes
# convert all rows as dictionary and convert the dataframe as a list
hibc_records = hibc.to_dict(orient="records")

for record in hibc_records[2:]:
    # Instantiate a new Metadata form with Coscine SDK method
    new_fasta = genomes.metadata_form()
    # Extract the medata from the record with a custom helper function
    mdata = get_metadata_from_df(record)
    # Fill the form with the metadata dictionary with Coscine SDK method
    new_fasta.fill(mdata)
    # Extract the path to the genome file with a custom helper function
    path_genome = find_the_genome_file(record)
    # Upload on Coscine witha SDK method expecting
    #  name of the object, path and Coscine SDK object with the filled metadata form
    genomes.upload(os.path.basename(path_genome), path_genome, new_fasta)

```


## A note on retrieving data from Coscine

At the time of writing this example, there is no way to fetch data when you are not part of the project. There is the possibility of being included in a project as a guest (see the [documentation](https://docs.coscine.de/en/projects/roles/)), but this was not satisfying for our need of distributing genomes to others researchers. 

We had two major constraints: 1) we could not include all future data-downloaders as guest and 2) our target audience will not embrace easily S3 clients as described in the [documentation](https://docs.coscine.de/en/rdm/resources/s3-clients/mini-io/).

So, our trick was to rely on the read-only token available for a RDS-S3 Resource, and use this token within a R S3 client within a Shiny application that would carry out the action of interfacing with the S3 Resource via the web browser of our target research audience.
