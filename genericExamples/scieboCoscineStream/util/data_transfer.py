import coscine
import logging
import requests
from util.sciebo_util import ScieboUtils


# create the log files
# TODO use the actual logger function
file_paths = [
    "logs/missing_files.log",
    "logs/completed_folders.log",
    "logs/verification_issues.log",
]

for file_path in file_paths:
    # Open the file in append mode; if it doesn't exist, it will be created
    with open(file_path, "a"):
        pass  # Just opening and closing the file to ensure it exists


class ScieboToCoscine:
    def __init__(
        self,
        cos: coscine.client.ApiClient,
        sc: ScieboUtils,  # TODO may want to change that naming...
        sciebo_folder: str,
    ) -> None:
        self.webdav_username = sc.webdav_session.auth.username
        self.webdav_password = sc.webdav_session.auth.password
        self.sc = sc
        self.cos = cos
        self.sciebo_folder = sciebo_folder

    def folderTransfer(
        self,
        resource: coscine.resource.Resource,
        filled_metadata_form: coscine.resource.MetadataForm,
        folder_name: str,
        full_content: list,
        re_verify: bool = False,
    ):
        """
        Prepares everything for transfer.
        Takes the specific single folder to transfer as well as a list of the full
        sciebo content.
        Also requires the resource in coscine to transfer to as well as
        the filled metadata form.
        At the very end, it assigns metadata to the folder in coscine.
        """
        noError = True
        existing_files = [
            f.path for f in resource.files(recursive=True) if not f.is_folder
        ]
        folder_files = [
            l for l in full_content if folder_name in l and not l[-1] == "/"
        ]
        for i, path in enumerate(folder_files):  # recursive, every i one file
            #print(f"file # {i}")  # sanity check
            if not self.fileTransfer(
                folder_name,
                path,
                filled_metadata_form,
                resource,
                existing_files,
                re_verify,
            ):
                noError = False
            
        if noError:
            self.completedFolders(folder_name)
        
        # assign metadata to the folder
        # this will only create new metadata, it will NOT overwrite it
        cos_folder = resource.file(folder_name)
        if not cos_folder.metadata():
            cos_folder.post_metadata(filled_metadata_form)

        return noError

    def fileTransfer(
        self,
        folder_name: str,
        path: str,
        filled_metadata_form: coscine.MetadataForm,
        resource: coscine.Resource,
        existing_files: list,
        re_verify: bool,
        retries=3,  # connections errors, etc.
    ) -> bool:
        """
        Does the actual file transfer.
        Creates the coscine path from the given sciebo path and folder name.
        After the upload, calls the verify function.
        Return True if all goes well, False if not.
        """
        #      print(folder_name)
        cos_path = path.split(self.sciebo_folder + "/")[-1]
        #       print(f"split path for coscine: {cos_path}")

        # check if file is already there
        #        print(f"path in existing coscinefiles:{cos_path in existing_files}")
        # print('')
        if cos_path not in existing_files:
            upload_complete = False
            while not upload_complete:
                try:
                    filled_metadata_form.validate()
                except ValueError:
                    filled_metadata_form = self.cos.metadata(
                        resource, folder_name
                    )
                try:
                    # Stream file content from WebDAV and upload to S3
                    url = "https://rwth-aachen.sciebo.de/" + path
                    print(f"transferring {url}")
                    with self.sc.webdav_session.get(
                        url, stream=True
                    ) as response:
                        resource.upload(
                            cos_path, response.content, filled_metadata_form
                        )

                    # remove from missing files if it's there
                    self.missingFiles(url, overwrite=True)
                    upload_complete = self.verify(resource, cos_path, path)
                    return upload_complete

                except Exception as err:
                    # for some reason it uploads and throws but throws and error even though everything is fine
                    # sadly, this costs another request
                    if cos_path in [
                        f.path
                        for f in resource.files(recursive=True)
                        if not f.is_folder
                    ]:
                        continue
                    if retries != 0:
                        logging.info(
                            "an error occurred when uploading, retrying..."
                        )
                        retries = retries - 1
                    else:
                        print(err)
                        print(err.with_traceback)
                        logging.error(
                            f"{path} not added, please verify. \ntype error:{type(err)}"
                        )
                        self.missingFiles(url)
                        return upload_complete
        else:
            url = "https://rwth-aachen.sciebo.de/" + path
            self.missingFiles(url, overwrite=True)
            return True
        if re_verify:  # checks if existing files are ok
            return self.verify(resource, cos_path, path)

    def completedFolders(self, folder_name: str):
        """
        logs the names of the folders that have been completely uploaded.
        """

        with open("logs/completed_folders.log", "r") as f:
            completed = [line.strip() for line in f.readlines()]

        if folder_name not in completed:
            with open("logs/completed_folders.log", "a") as log:
                log.write(f"{folder_name}\n")

    def verify(
        self,
        resource: coscine.Resource,
        cos_path: str,
        path: str,
        size_diff: int = 0,
    ) -> bool:
        """
        Compares the size of the file iin Sciebo that in Coscine.
        Returns True is they match
        (within a given deviation, defined by size_diff, default 0),
        False if they don't.
        Gives an info if Sciebo says 0 but Coscine has a siz
        Any differences are logged in the verification_issues.log
        """
        retries = 3
        while retries != 0:
            try:
                cos_file_size = resource.file(cos_path).size
                sciebo_file_size = self.sc.getFileSize(
                    self.webdav_username, self.webdav_password, path
                )

                if sciebo_file_size == 0 and cos_file_size != 0:
                    logging.warning(
                        f"""
                                    size difference in {path}
                                    sciebo file size: {sciebo_file_size}
                                    coscine file size: {cos_file_size}
                            """
                    )
                    self.verificationLog(
                        cos_file_size, sciebo_file_size, resource, cos_path
                    )
                    return True  # this case doesn't seem to be a problem
                elif abs(cos_file_size - sciebo_file_size) > size_diff:
                    logging.warning(
                        f"""
                                    size difference in {path}
                                    sciebo file size: {sciebo_file_size}
                                    coscine file size: {cos_file_size}
                            """
                    )

                    self.verificationLog(
                        cos_file_size, sciebo_file_size, resource, cos_path
                    )
                    return False
                else:  # all is good, sizes match within limit
                    return True

            except coscine.exceptions.NotFoundError as err:
                logging.error(
                    f'coscine "not found" error, check {path} for {path.split("/")[6]}. \n{err}'
                )
                return False
            except requests.exceptions.ConnectTimeout as err:
                if retries == 0:
                    logging.error(
                        f"too many retries during verification for for {path}: {err.with_traceback}"
                    )
                else:
                    logging.error(
                        f"connection error, retrying ... ({retries} retries)"
                    )
                    retries = retries - 1
                    continue
            # except Exception as err:
            #     logging.error(f'error during verification for for {path}: {err.with_traceback}')
            #     logging.error(type(err))
            #     return False

    def verificationLog(
        self,
        cos_file_size,
        sciebo_file_size,
        resource: coscine.Resource,
        cos_path: str,
    ):
        """
        logs any files that don't pass the verification
        """
        with open("logs/verification_issues.log", "a") as log:
            log.write(
                f"Size difference exceeds limit, check {cos_path} in {resource.name}\n"
            )
            log.write(f"coscine file fize: {cos_file_size}\n")
            log.write(f"sciebo file fize: {sciebo_file_size}\n\n")

    def missingFiles(self, url: str, overwrite=False):
        """
        when files won't upload, lists them in a file.
        """
        with open(
            "logs/missing_files.log", "r"
        ) as f:  # make sure we're updating
            missing_files = [line.strip() for line in f.readlines()]
        if overwrite == True:
            if url in missing_files:
                missing_files.remove(url)
                with open("logs/missing_files.log", "w") as log:  # overwrite
                    for item in missing_files:
                        log.write(f"{item}\n")
        else:
            if url not in missing_files:
                missing_files.append(url)
                with open("logs/missing_files.log", "a") as log:
                    log.write(f"{url}\n")
