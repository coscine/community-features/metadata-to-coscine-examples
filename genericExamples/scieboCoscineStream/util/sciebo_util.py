import requests
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
from urllib.parse import unquote
import logging

logging.basicConfig(level=logging.INFO)


# may need to rethink a bit for the generic case (single folder upload)
class ScieboUtils:
    def __init__(self, webdav_username, webdav_password, webdav_base_url):
        self.webdav_base_url = webdav_base_url
        self.webdav_session = requests.Session()
        self.webdav_session.auth = HTTPBasicAuth(
            webdav_username, webdav_password
        )

        self.check_connection()

    def getSession(self):
        return self.webdav_session

    def check_connection(self) -> bool:
        """
        Check if the connection is valid and working.

        Returns:
            bool: True if the connection is valid, False otherwise
        """
        # Make a quick HEAD request to the base URL to check if the connection is valid
        response = self.webdav_session.head(self.webdav_base_url)

        if response.status_code == 200:
            logging.info("Connection to Sciebo successful")
            return True
        else:
            logging.error("Connection to Sciebo failed")
            return False

    def folderResponseRoot(self, folder_path):

        webdav_folder_url = self.webdav_base_url.rstrip("/") + folder_path
        webdav_response = self.webdav_session.request(
            "PROPFIND", webdav_folder_url
        )
        # Parse the XML response using ElementTree
        root = ET.fromstring(webdav_response.content)
        return root

    def toplevelContent(self, root):
        # Extract files and folders in the current directory
        content = [
            child.text for child in root.iter("{DAV:}href") if child.text
        ]
        # fix any decoding problems:
        content = self.fixDecoding(content)
        return content

    def getFileSize(self, username, password, file_path):
        url = "https://rwth-aachen.sciebo.de/" + file_path
        response = requests.head(url, auth=HTTPBasicAuth(username, password))

        if response.status_code == 200:
            size = int(response.headers.get("Content-Length", 0))
            return size
        else:
            logging.critical(
                f"Failed to retrieve file information. Status code: {response.status_code}"
            )
            return None

    def findNestedFolder(self, root, folder_path):
        nested_folders = []
        folder_name = folder_path.split("/")[-1]

        for child in root.iter(
            "{DAV:}href"
        ):  # this is not finding things that are collections
            if (
                child.text[-1] == "/"
                and child.text.split("/")[-2] != folder_name
            ):
                nested_folder = child.text.split("/")[-2]
                nested_folder_path = folder_path + "/" + nested_folder
                nested_folders.append(nested_folder_path)
        return nested_folders

    def getAllInfo(
        self, current_folder: str, sciebo_content: list, folders: bool
    ):
        root = self.folderResponseRoot(current_folder)
        found_files_and_folders = self.toplevelContent(root)
        if folders:
            sciebo_content += found_files_and_folders
        else:
            for item in found_files_and_folders:
                if "." in str(item).split("/")[-1]:  # if it's not a folder
                    sciebo_content.append(unquote(item))
        nested_folders = self.findNestedFolder(root, current_folder)
        return nested_folders

    def fixDecoding(
        self,
        encoded_strings: list,
    ):  # could also do this on a string by string basis...
        decoded_strings = [
            unquote(encoded_str) for encoded_str in encoded_strings
        ]
        return decoded_strings

    def getFolderSizes(self, top_folder: str) -> dict:
        size_dict = {}
        root = self.folderResponseRoot(top_folder)
        # Define the DAV namespace
        dav_namespace = {"dav": "DAV:"}
        # Extract and sum the size of each item in the folder
        for item in root.iterfind(".//dav:response", namespaces=dav_namespace):
            href = item.find("dav:href", namespaces=dav_namespace).text
            size_element = item.find(
                ".//dav:quota-used-bytes", namespaces=dav_namespace
            )
            # Check if the size information is available
            if size_element is not None:
                size = int(size_element.text)
                folder_name = unquote(href.split("/")[-2])
                size_dict[folder_name] = size / (
                    1024**3
                )  # need to convert to GB
        return size_dict

    def getAllFiles(self, sciebo_folder: str, return_folder=True) -> list:
        print("getting sciebo content...")
        sciebo_content = []
        nested_folders = self.getAllInfo(
            sciebo_folder, sciebo_content, return_folder
        )
        while nested_folders:
            additional_folders = []
            for folder in nested_folders:
                additional_folders += self.getAllInfo(
                    folder, sciebo_content, return_folder
                )
            if additional_folders:
                nested_folders = additional_folders
            else:
                nested_folders = False

        # fix any decoding problems:
        # sciebo_content = self.fixDecoding(sciebo_content)
        print(f"total files in sciebo folder: {len(sciebo_content)}")
        return sciebo_content

    def scieboTopFolders(self, main_sciebo_folder: str):
        root = self.folderResponseRoot(main_sciebo_folder)
        top_level_folders = self.toplevelContent(root)

        # cleanup (remove the very toplevel folder and any files from list)
        items_to_remove = []
        for item in top_level_folders:
            if item[-1] != "/":
                items_to_remove.append(item)
            elif item.split("/")[-2] == main_sciebo_folder.split("/")[-1]:
                items_to_remove.append(item)
        for item in items_to_remove:
            top_level_folders.remove(item)
        return top_level_folders

    def getFile(self, path, save_path):

        path_url = "https://rwth-aachen.sciebo.de/" + path
        response = self.webdav_session.get(path_url, stream=True)

        # Check if the request was successful
        if response.status_code == 200:
            # Path where you want to save the downloaded file
            output_file_path = "path/to/save/file"

            # Open the file in write-binary mode and save the content
            with open(save_path, "wb") as file:
                for chunk in response.iter_content(chunk_size=8192):
                    if chunk:
                        file.write(chunk)
            print(f"{save_path}  downloaded successfully")
        else:
            print(
                f"Failed to download {save_path}. Status code: {response.status_code}"
            )
