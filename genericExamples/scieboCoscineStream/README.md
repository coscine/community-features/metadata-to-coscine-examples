# Stream Data from Sciebo to Coscine

This folder contains code in [util](util) that can be used to stream data from Sciebo to Coscine.

The Notebook [ScieboCoscine_playground.ipynb](ScieboCoscine_playground.ipynb) shows and example of how this can be used. See this notebook for details.